# Fizzbuzz

This is a sample project implementing various JavaScript algorithms for the well-known FizzBuzz problem:

> For a monotonic sequence of positive integers of some maximum length, beginning at `1`, replace each multiple of `3` with `fizz` and each multiple of `5` with `buzz`, except for integers where both `3` and `5` are factors, in which case substitute `fizzbuzz`.

Various design goals are emphasized in these algorithms:

- simplicity / readability
- functional programming
- performance optimization

## Implementation

This project also demonstrates the use of [ES6 modules](https://developers.google.com/web/fundamentals/primers/modules) for both [Node.js](Node.js) and modern web browsers. The JavaScript source code is annotated with [flow](https://flow.org/) types, with build scripts provided for static analysis and style checking using [eslint](https://eslint.org). Test cases are defined with [mocha](https://mochajs.org) and [chai](chaijs.com).