import { assert } from 'chai';
import { optimized } from '../app/optimized.mjs';


describe('optimized Fizzbuzz tests', () => {
  it('optimized() result is type Array', () => {
    const result = optimized();
    assert.isArray(result, 'optimized() result is not type Array');
  });
  it('default optimized() result is array of length 32', () => {
    const result = optimized();
    assert.equal(result.length, 32, 'optimized() result is not an array of length 32');
  });
  it('optimized(15) result is expected array', () => {
    const expectedResult = [
      '1', '2', 'fizz',
      '4', 'buzz', 'fizz',
      '7', '8', 'fizz',
      'buzz', '11', 'fizz',
      '13', '14', 'fizzbuzz'
    ];
    const result = optimized(15);
    assert.deepEqual(result, expectedResult, 'optimized(15) result is not expected array');
  });
  it('optimized(0) result is expected null array', () => {
    const expectedResult = [];
    const result = optimized(0);
    assert.deepEqual(result, expectedResult, 'optimized(0) result is not null array');
  });
  it('optimized(-1) result is expected null array', () => {
    const expectedResult = [];
    const result = optimized(0);
    assert.deepEqual(result, expectedResult, 'optimized(-1) result is not null array');
  });
});

//# sourceMappingURL=optimized-test.js.map
