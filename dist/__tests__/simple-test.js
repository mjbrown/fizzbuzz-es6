import { assert } from 'chai';
import { simple } from '../app/simple.mjs';


describe('simple Fizzbuzz tests', () => {
  it('simple() result is type Array', () => {
    const result = simple();
    assert.isArray(result, 'simple() result is not type Array');
  });
  it('default simple() result is array of length 32', () => {
    const result = simple();
    assert.equal(result.length, 32, 'simple() result is not an array of length 32');
  });
  it('simple(15) result is expected array', () => {
    const expectedResult = [
      '1', '2', 'fizz',
      '4', 'buzz', 'fizz',
      '7', '8', 'fizz',
      'buzz', '11', 'fizz',
      '13', '14', 'fizzbuzz'
    ];
    const result = simple(15);
    assert.deepEqual(result, expectedResult, 'simple(15) result is not expected array');
  });
  it('simple(0) result is expected null array', () => {
    const expectedResult = [];
    const result = simple(0);
    assert.deepEqual(result, expectedResult, 'simple(0) result is not null array');
  });
  it('simple(-1) result is expected null array', () => {
    const expectedResult = [];
    const result = simple(0);
    assert.deepEqual(result, expectedResult, 'simple(-1) result is not null array');
  });
});

//# sourceMappingURL=simple-test.js.map
