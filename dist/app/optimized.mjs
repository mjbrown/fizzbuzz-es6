//      

/*
 * Optimized fizbuzz sequence production
 */
export function optimized(max          = 32)           {
  console.log('\nOptimized fizzbuzz:');
  console.time('Optimized fizzbuzz');
  const result = Array(0);
  for (let idx = 1; idx <= max; idx += 1) {
    if ((idx % 3) === 0) {
      if ((idx % 5) === 0) {
        result.push('fizzbuzz');
      } else {
        result.push('fizz');
      }
    } else if ((idx % 5) === 0) {
      result.push('buzz');
    } else {
      result.push(String(idx));
    }
  }
  console.log(...result);
  console.timeEnd('Optimized fizzbuzz');
  return result;
}

export default optimized;

//# sourceMappingURL=optimized.mjs.map
