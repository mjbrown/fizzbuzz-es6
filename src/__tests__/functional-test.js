import { assert } from 'chai';
import { functional } from '../app/functional.mjs';


describe('functional Fizzbuzz tests', () => {
  it('functional() result is type Array', () => {
    const result = functional();
    assert.isArray(result, 'functional() result is not type Array');
  });
  it('default functional() result is array of length 32', () => {
    const result = functional();
    assert.equal(result.length, 32, 'functional() result is not an array of length 32');
  });
  it('functional(15) result is expected array', () => {
    const expectedResult = [
      '1', '2', 'fizz',
      '4', 'buzz', 'fizz',
      '7', '8', 'fizz',
      'buzz', '11', 'fizz',
      '13', '14', 'fizzbuzz'
    ];
    const result = functional(15);
    assert.deepEqual(result, expectedResult, 'functional(15) result is not expected array');
  });
  it('functional(0) result is expected null array', () => {
    const expectedResult = [];
    const result = functional(0);
    assert.deepEqual(result, expectedResult, 'functional(0) result is not null array');
  });
  it('functional(-1) result is expected null array', () => {
    const expectedResult = [];
    const result = functional(0);
    assert.deepEqual(result, expectedResult, 'functional(-1) result is not null array');
  });
});
