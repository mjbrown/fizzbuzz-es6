// @flow

import { simple } from './app/simple.mjs';
import { functional } from './app/functional.mjs';
import { optimized } from './app/optimized.mjs';

/*
** Run fizzbuzz sequence productions
*/

simple(30);
functional(30);
optimized(30);
