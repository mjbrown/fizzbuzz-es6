// @flow

/*
 * Functional fizbuzz sequence production
 */
export function functional(max?: number = 32): ?string[] {
  console.log('\nFunctional fizzbuzz:');
  console.time('Functional fizzbuzz');
  const result = Array.from(
    { length: max },
    (val, idx) => idx + 1
  ).map(val => (((typeof val === 'number')
    && !((val % 3) || (val % 5)))
    ? 'fizzbuzz'
    : val
  )).map(val => (((typeof val === 'number')
    && !(val % 3))
    ? 'fizz'
    : val
  )).map(val => (((typeof val === 'number')
    && !(val % 5))
    ? 'buzz'
    : String(val)
  ));
  console.log(...result);
  console.timeEnd('Functional fizzbuzz');
  return result;
}

export default functional;
