// @flow

/*
 * Simple fizbuzz sequence production
 */
export function simple(max?: number = 32): string[] {
  console.log('\nSimple fizzbuzz:');
  console.time('Simple fizzbuzz');
  const result = Array(0);
  for (let idx = 1; idx <= max; idx += 1) {
    if ((idx % 3) === 0 && (idx % 5) === 0) {
      result.push('fizzbuzz');
    } else if ((idx % 3) === 0) {
      result.push('fizz');
    } else if ((idx % 5) === 0) {
      result.push('buzz');
    } else {
      result.push(String(idx));
    }
  }
  console.log(...result);
  console.timeEnd('Simple fizzbuzz');
  return result;
}

export default simple;
